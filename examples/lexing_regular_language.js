/*
This is an example showing using TagLex purely for tokenizing.

The "state" feature in this case has no stack, because we are using the Lexer
class instead of a Parser class.
*/

var taglex = require('taglex');
var sys = require('sys');

var ruleset = new taglex.RuleSet({ ignore_case: true });
ruleset.add("."); // taglex.ROOT, taglex.NOOP is default
ruleset.add("*");
ruleset.add("/*", taglex.ROOT, "comment");
ruleset.add("*/", "comment", taglex.ROOT);

var lexer = ruleset.new_lexer();

lexer.on('token', function (type, value) {
    if (type === taglex.TAG_NODE) {
        if (value === ".") {
            sys.print(","); // replace "." with ","
        }
        // ignore "*", "/*", and "*/"

    } else {
        if (lexer.state !== "comment") {
            // don't print text nodes within comments
            sys.print(value);
        }
    }
});

lexer.write("A simple. Lexing. ***Example.\n")
lexer.write("/* Ignore me... */Back... to root state");

// Expected results (as a list, instead  of new lines)
exports.expected = [
    "A simple, Lexing, Example,",
    "Back,,, to root state",
];

