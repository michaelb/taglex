var taglex = require('./index');

exports.test_lexer = function (test) {
    var ruleset = new taglex.RuleSet();

    var result, parser, text, contents;
    var emit = function (t, c) { contents.push([t, c]); };

    // Add in two rules
    // Add paragraph X emphasis relation
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('*', 'tag_emphasis', taglex.POP);

    // Add paragraph X underline relation
    ruleset.add('u{', 'tag_paragraph', 'tag_underline');
    ruleset.add('}', 'tag_underline', taglex.POP);

    // underline can contain emphasis, but not the other way around
    ruleset.add('*', 'tag_underline', 'tag_emphasis');

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a *b c d* e f u{g h *i j* k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*' ], [ 1, 'b c d' ], [ 2, '*' ],
                [ 1, ' e f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    /////////////// TEST 2
    // ensure we cannot have underline within emphasis, also case sensitive
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a *b u{c} d* e f u{g h *i j* k} l U{m} n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*' ], [ 1, 'b u{c} d' ], [ 2, '*' ],
                [ 1, ' e f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l U{m} n o' ] ];
    test.deepEqual(result, contents);


    /////////////// TEST 3
    // ensure arbitrary multiple writes work fine
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    var texts = ["a *", "b u{c} d*",
            " e f u{", "g h *i", " j* k", "} l U{", "m} n o", ""];
    for (var i in texts) { parser.write(texts[i]); }
    result = [ [ 1, 'a ' ],
                [ 2, '*' ], [ 1, 'b u{c} d' ], [ 2, '*' ],
                [ 1, ' e f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i' ], [ 1, ' j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l U{' ], [ 1, 'm} n o' ],
            ];
    test.deepEqual(result, contents);


    /////////////// TEST 4
    // many short edge cases
    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [ [ 2, '*' ], [ 1, 'test' ], [ 2, '*' ], ];
    parser.write("*test*");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 2, 'u{' ], [ 1, 'test' ], [ 2, '}' ], ];
    parser.write("u{test}");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 1, 'test' ], ];
    parser.write("test");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 2, '*' ], [ 2, '*' ], ];
    parser.write("**");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 2, 'u{' ], [ 2, '*' ], [ 2, '*' ], [ 2, '*' ], [ 2, '*' ], [ 2, '}' ], ];
    parser.write("u{****}");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 2, 'u{' ], [ 2, '*' ], [ 2, '*' ], [ 2, '*' ], [ 2, '*' ], [ 2, '*' ], [ 1, '}' ], ];
    parser.write("u{*****}");
    test.deepEqual(result, contents);

    contents = []; parser = ruleset.new_parser('tag_paragraph', emit);
    result = [  [ 2, 'u{' ], [ 2, '*' ], [ 1, '}' ], [ 2, '*' ], [ 2, '}' ], ];
    parser.write("u{*}*}");
    test.deepEqual(result, contents);

    /////////////// TEST 5
    // test prefixes, add in strong too
    ruleset.add('**', 'tag_paragraph', 'tag_strong');
    ruleset.add('**', 'tag_emphasis', 'tag_strong');
    ruleset.add('**', 'tag_strong', taglex.POP);
    ruleset.add('*', 'tag_strong', 'tag_emphasis');
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a} **b c d** e f **g h *i j* k*** l m u{n o";
    parser.write(text);
    result = [ [ 1, 'a} ' ],
                [ 2, '**' ], [ 1, 'b c d' ], [ 2, '**' ],
                [ 1, ' e f ' ],
                [ 2, '**' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '**' ],
                    [ 2, '*' ],
                [ 1, ' l m u{n o' ] ];
    test.deepEqual(result, contents);


    /////////////// TEST 6
    // test recursion, make underline recursive
    ruleset.add('u{', 'tag_underline', 'tag_underline');
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a} u{b c d} e f u{g h u{i j} k} l u{u{u{u{m}}}** n} ** o **";
    parser.write(text);
    result = [ [ 1, 'a} ' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, 'u{' ], [ 1, 'i j' ], [ 2, '}' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l ' ],
                    [ 2, 'u{' ], [ 2, 'u{' ], [ 2, 'u{' ], [ 2, 'u{' ],
                                        [ 1, 'm' ],
                            [ 2, '}' ], [ 2, '}' ], [ 2, '}' ],
                        [ 2, '*' ], [ 2, '*' ],
                        [ 1, ' n' ],
                    [ 2, '}' ], [ 1, ' ' ],
                    [ 2, '**' ], [ 1, ' o ' ], [ 2, '**' ],
                ];
    test.deepEqual(result, contents);

    /////////////// TEST 7
    // test descendant and parent state helper functions
    test.deepEqual(ruleset.get_descendant_states("tag_underline").sort(),
        ["tag_emphasis", "tag_strong", "tag_underline"]);

    test.deepEqual(ruleset.get_ancestor_states("tag_underline").sort(),
        ["tag_paragraph", "tag_underline"]);

    test.deepEqual(ruleset.get_descendant_states("tag_emphasis").sort(),
        ["tag_emphasis", "tag_strong"]);

    test.deepEqual(ruleset.get_ancestor_states("tag_emphasis").sort(),
        ["tag_emphasis", "tag_paragraph", "tag_strong", "tag_underline"]);

    test.deepEqual(ruleset.get_descendant_states("tag_strong").sort(),
        ["tag_emphasis", "tag_strong"]);

    test.deepEqual(ruleset.get_ancestor_states("tag_strong").sort(),
        ["tag_emphasis", "tag_paragraph", "tag_strong", "tag_underline"]);

    test.deepEqual(ruleset.get_descendant_states("tag_paragraph").sort(),
        ["tag_emphasis", "tag_strong", "tag_underline"]);

    test.deepEqual(ruleset.get_ancestor_states("tag_paragraph").sort(),
        []);

    test.done();
};

exports.test_lexer_overlapping_tokens = function (test) {
    var ruleset = new taglex.RuleSet();

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('*', 'tag_emphasis', taglex.POP);
    ruleset.add('u{', 'tag_paragraph', 'tag_underline');
    ruleset.add('}', 'tag_underline', taglex.POP);
    ruleset.add('*', 'tag_underline', 'tag_emphasis');

    // add in new rules
    ruleset.add('k{', 'tag_paragraph', 'tag_k');
    ruleset.add('}', 'tag_k', taglex.POP);
    ruleset.add('j{', 'tag_paragraph', 'tag_j');
    ruleset.add('}', 'tag_j', taglex.POP);
    ruleset.add('j{', 'tag_j', 'tag_j2');
    ruleset.add('};', 'tag_j2', taglex.POP);

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a u{g h *I J* k}u{b c d}k{}j{} e f j{g h j{I } J}; k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, 'u{'], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'I J' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 2, 'k{' ], [ 2, '}' ],
                [ 2, 'j{' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'j{'], [ 1, 'g h ' ],
                    [ 2, 'j{' ], [ 1, 'I } J' ], [ 2, '};' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);
    test.done();
};


exports.test_lexer_stack_pass = function (test) {
    var ruleset = new taglex.RuleSet();

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('--', 'tag_emphasis', taglex.NOOP);
    ruleset.add('*', 'tag_emphasis', taglex.POP);

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a *g h --I J k* l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*'], [ 1, 'g h ' ],
                    [ 2, '--' ], [ 1, 'I J k' ],
                    [ 2, '*' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    test.done();
};


exports.test_lexer_empty_match = function (test) {
    var ruleset = new taglex.RuleSet();

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('', 'tag_emphasis', 'tag_empty');
    ruleset.add('|', 'tag_empty', taglex.POP);
    ruleset.add('*', 'tag_empty', taglex.DOUBLE_POP);


    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a *g h |I J k* l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*'],
                    [ 2, '' ],
                    [ 1, 'g h ' ],
                    [ 2, '|' ],
                    [ 2, '' ],
                    [ 1, 'I J k' ],
                    [ 2, '*' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    ruleset.add('?', 'tag_emphasis', 'tag_alt');
    ruleset.add('*', 'tag_alt', taglex.DOUBLE_POP);

    /////////////// TEST 2
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a *g h |?I J k* l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*'],
                    [ 2, '' ],
                    [ 1, 'g h ' ],
                    [ 2, '|' ],
                    [ 2, '?' ],
                    [ 1, 'I J k' ],
                    [ 2, '*' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);


    /////////////// TEST 3, starting empty
    ruleset.add('', 'tag_root', 'new_state');
    ruleset.add('test', 'new_state', 'new_state');

    contents = [];
    parser = ruleset.new_parser('tag_root', emit);
    text = "asdf";
    parser.write(text);
    result = [ [ 2, '' ], [ 1, 'asdf'] ];
    test.deepEqual(result, contents);
    test.done();

};

exports.test_lexer_ignore_case = function (test) {
    var ruleset = new taglex.RuleSet({ignore_case: true});

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('*', 'tag_emphasis', taglex.POP);
    ruleset.add('u{', 'tag_paragraph', 'tag_underline');
    ruleset.add('}', 'tag_underline', taglex.POP);
    ruleset.add('*', 'tag_underline', 'tag_emphasis');

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a u{b c d} e f U{g h *I J* k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'u{', 'U{', ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'I J' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    test.done();
};



exports.test_lexer_regexp = function (test) {
    var ruleset = new taglex.RuleSet({
        normalizer: function (v) { return v.replace(/^_+/, '_'); }
    });

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('_', 'tag_paragraph', 'tag_emphasis', '_+');
    ruleset.add('_', 'tag_emphasis', taglex.POP, '_+');
    ruleset.add('u{', 'tag_paragraph', 'tag_underline');
    ruleset.add('}', 'tag_underline', taglex.POP);
    ruleset.add('_', 'tag_underline', 'tag_emphasis', '_+');

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a ___b c d__ e f u{g h __I J_ k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '_', '___', ], [ 1, 'b c d' ], [ 2, '_', '__' ],
                [ 1, ' e f ' ],
                [ 2, 'u{', ], [ 1, 'g h ' ],
                    [ 2, '_', '__' ], [ 1, 'I J' ], [ 2, '_' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    test.done();
};


exports.test_lexer_collapse = function (test) {
    var ruleset = new taglex.RuleSet();

    var result, parser, text, contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    // Add in old rules
    ruleset.add('*', 'tag_paragraph', 'tag_emphasis');
    ruleset.add('*', 'tag_emphasis', taglex.POP);
    ruleset.add('u{', 'tag_paragraph', 'tag_underline');
    ruleset.add('}', 'tag_underline', taglex.POP);
    ruleset.add('*', 'tag_underline', 'tag_emphasis');
    // Add in collapser for 
    ruleset.add_stack_collapser('}', 'tag_underline');

    /////////////// TEST 1
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a u{b c d} e f u{g h *I J k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'u{', ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'I J k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);


    /////////////// TEST 2 (extra non-collapse rules)
    // add in new rules
    ruleset.add('k{', 'tag_paragraph', 'tag_k');
    ruleset.add('k{', 'tag_underline', 'tag_k');
    ruleset.add('}', 'tag_k', taglex.POP);
    ruleset.add('j{', 'tag_paragraph', 'tag_j');
    ruleset.add('j{', 'tag_underline', 'tag_j');
    ruleset.add('}', 'tag_j', taglex.POP);
    ruleset.add('j{', 'tag_j', 'tag_j2');
    ruleset.add('};', 'tag_j2', taglex.POP);


    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a u{g h *I J k}u{b c d}k{}j{} e f j{g h j{I } J}; k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, 'u{'], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'I J k' ],
                    [ 2, '}' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 2, 'k{' ], [ 2, '}' ],
                [ 2, 'j{' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'j{'], [ 1, 'g h ' ],
                    [ 2, 'j{' ],

                    [ 1, 'I }'], [ 1, ' J' ], // NOTE: XXX anti-feature, too many text nodes

                    [ 2, '};' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);


    /////////////// TEST 3 (trickier interweaving of similar tokens)
    // add in new rules
    contents = [];
    parser = ruleset.new_parser('tag_paragraph', emit);
    text = "a u{g h j{I J k}u{b c d}}k{}j{} e f j{g h j{I } J}; k} l m n o";
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, 'u{'], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'I J' ], [ 2, 'j{' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 2, '}' ],
                [ 2, 'k{' ], [ 2, '}' ],
                [ 2, 'j{' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'j{'], [ 1, 'g h ' ],
                    [ 2, 'j{' ], 
                    [ 1, 'I }'], [ 1, ' J' ], // NOTE: XXX anti-feature, too many text nodes
                    [ 2, '};' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.done();
};

exports.test_tag_parser_basic = function (test) {
    var result, parser, text, contents, node_contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    var setup_tagparser = function (parser) {
        node_contents = [];
        parser.on('tag_open', function (tagname) { node_contents.push(['OPEN', tagname]); });
        parser.on('tag_close', function (tagname) { node_contents.push(['CLOSE', tagname]); });
        parser.on('text_node', function (val) { node_contents.push(['TEXT', val]); });
        parser.on('symbol', function (val) { node_contents.push(['SYMBOL', val]); });
    };


    var ruleset = new taglex.TagRuleSet();
    ruleset.add_tag({ name: 'emphasis', open: '*', close: '*', parents: ['underline', 'strong', 'root'], });
    ruleset.add_tag({ name: 'underline', open: 'u{', close: '}', parents: ['root', 'underline'], });
    ruleset.add_tag({ name: 'strong', open: '**', close: '**', parents: ['root', 'emphasis'], });
    ruleset.add_symbol({ name: 'dash', symbol: '---', parents: ['root', 'underline', 'emphasis'], });

    /////////////// TEST 1
    contents = []; node_contents = [];
    parser = ruleset.new_parser(null, emit);
    text = "a *b c d---* e--- f u{g h *i j* k} l m n o";
    setup_tagparser(parser);
    parser.write(text);
    result = [ [ 1, 'a ' ],
                [ 2, '*' ], [ 1, 'b c d' ], [ 2, '---' ], [ 2, '*' ],
                [ 1, ' e' ], [ 2, '---' ], [ 1, ' f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l m n o' ] ];
    test.deepEqual(result, contents);

    // check node emissions
    result = [ [ 'TEXT', 'a ' ],
                [ 'OPEN', 'emphasis' ], [ 'TEXT', 'b c d' ], [ 'SYMBOL', 'dash'], [ 'CLOSE', 'emphasis' ],
                [ 'TEXT', ' e'], [ 'SYMBOL', 'dash'],
                [ 'TEXT', ' f ' ],
                [ 'OPEN', 'underline' ], [ 'TEXT', 'g h ' ],
                    [ 'OPEN', 'emphasis' ], [ 'TEXT', 'i j' ], [ 'CLOSE', 'emphasis' ],
                    [ 'TEXT', ' k' ], [ 'CLOSE', 'underline' ],
                [ 'TEXT', ' l m n o' ] ];
    test.deepEqual(result, node_contents);


    /////////////// TEST 2
    // test strong too
    contents = [];
    parser = ruleset.new_parser(null, emit);
    text = "a} **b c d** e f **g h *i j* k*** l m u{n o";
    setup_tagparser(parser);
    parser.write(text);
    result = [ [ 1, 'a} ' ],
                [ 2, '**' ], [ 1, 'b c d' ], [ 2, '**' ],
                [ 1, ' e f ' ],
                [ 2, '**' ], [ 1, 'g h ' ],
                    [ 2, '*' ], [ 1, 'i j' ], [ 2, '*' ],
                    [ 1, ' k' ], [ 2, '**' ],
                    [ 2, '*' ],
                [ 1, ' l m u{n o' ] ];
    test.deepEqual(result, contents);

    // check node emissions
    result = [ [ 'TEXT', 'a} ' ],
                [ 'OPEN', 'strong' ], [ 'TEXT', 'b c d' ], [ 'CLOSE', 'strong' ],
                [ 'TEXT', ' e f ' ],
                [ 'OPEN', 'strong' ], [ 'TEXT', 'g h ' ],
                    [ 'OPEN', 'emphasis' ], [ 'TEXT', 'i j' ], [ 'CLOSE', 'emphasis' ],
                    [ 'TEXT', ' k' ], [ 'CLOSE', 'strong' ],
                    [ 'OPEN', 'emphasis' ],
                [ 'TEXT', ' l m u{n o' ] ];
    test.deepEqual(result, node_contents);


    /////////////// TEST 3
    // test recursion, make underline recursive
    contents = [];
    parser = ruleset.new_parser(null, emit);
    text = "a} u{b c d} e f u{g h u{i j} k} l u{u{u{u{m}}}** n} ** o **";
    parser.write(text);
    result = [ [ 1, 'a} ' ],
                [ 2, 'u{' ], [ 1, 'b c d' ], [ 2, '}' ],
                [ 1, ' e f ' ],
                [ 2, 'u{' ], [ 1, 'g h ' ],
                    [ 2, 'u{' ], [ 1, 'i j' ], [ 2, '}' ],
                    [ 1, ' k' ], [ 2, '}' ],
                [ 1, ' l ' ],
                    [ 2, 'u{' ], [ 2, 'u{' ], [ 2, 'u{' ], [ 2, 'u{' ],
                                        [ 1, 'm' ],
                            [ 2, '}' ], [ 2, '}' ], [ 2, '}' ],
                        [ 2, '*' ], [ 2, '*' ],
                        [ 1, ' n' ],
                    [ 2, '}' ], [ 1, ' ' ],
                    [ 2, '**' ], [ 1, ' o ' ], [ 2, '**' ],
                ];
    test.deepEqual(result, contents);

    test.done();
};


exports.test_tag_parser_root_ignore_text = function (test) {
    var result, parser, text, contents, node_contents;
    var emit = function (t, c, z) {
        var e = [t, c];
        if (z && z !== c) { e.push(z); }
        contents.push(e);
    };

    var setup_tagparser = function (parser) {
        node_contents = [];
        parser.on('tag_open', function (tagname) { node_contents.push(['OPEN', tagname]); });
        parser.on('tag_close', function (tagname) { node_contents.push(['CLOSE', tagname]); });
        parser.on('text_node', function (val) { node_contents.push(['TEXT', val]); });
        parser.on('symbol', function (val) { node_contents.push(['SYMBOL', val]); });
    };


    var ruleset = new taglex.TagRuleSet({
        root_ignore_text: true,
    });
    ruleset.add_tag({ name: 'emphasis', open: '*', close: '*', parents: ['underline', 'strong', 'root'], });
    ruleset.add_tag({ name: 'underline', open: 'u{', close: '}', parents: ['root', 'underline'], });
    ruleset.add_tag({ name: 'strong', open: '**', close: '**', parents: ['root', 'emphasis'], });
    ruleset.add_symbol({ name: 'dash', symbol: '---', parents: ['root', 'underline', 'emphasis'], });

    /////////////// TEST 1
    contents = []; node_contents = [];
    parser = ruleset.new_parser(null, emit);
    text = "a *b c d---* e--- f u{g h *i j* k} l m n o";
    setup_tagparser(parser);
    parser.write(text);

    // check node emissions
    result = [ 
                [ 'OPEN', 'emphasis' ], [ 'TEXT', 'b c d' ], [ 'SYMBOL', 'dash'], [ 'CLOSE', 'emphasis' ],
                [ 'SYMBOL', 'dash'],
                [ 'OPEN', 'underline' ], [ 'TEXT', 'g h ' ],
                    [ 'OPEN', 'emphasis' ], [ 'TEXT', 'i j' ], [ 'CLOSE', 'emphasis' ],
                    [ 'TEXT', ' k' ], [ 'CLOSE', 'underline' ],
                ];
    test.deepEqual(result, node_contents);
    test.done();
};




exports.test_tag_parser_event_buffering = function (test) {
    var result, parser, text, contents, node_contents, bufferer;
    var source_buffer_result = [];

    var setup_bufferer = function (b) {
        node_contents = [];
        b.on('source_buffer', function (v) { source_buffer_result.push(v); });
        b.on('tag_open', function (tagname) { node_contents.push(['OPEN', tagname]); });
        b.on('tag_close', function (tagname) { node_contents.push(['CLOSE', tagname]); });
        b.on('text_node', function (val) { node_contents.push(['TEXT', val]); });
        b.on('symbol', function (val) { node_contents.push(['SYMBOL', val]); });
    };

    var ruleset = new taglex.TagRuleSet();
    ruleset.add_tag({ name: 'emphasis',  open: '*',  close: '*',  parents: ['underline', 'strong', 'root'], });
    ruleset.add_tag({ name: 'underline', open: 'u{', close: '}',  parents: ['root', 'underline'], });
    ruleset.add_tag({ name: 'strong',    open: '**', close: '**', parents: ['root', 'emphasis'], });
    ruleset.add_symbol({ name: 'dash', symbol: '---', parents: ['root', 'underline', 'emphasis'], });

    /////////////// TEST 1
    contents = []; node_contents = [];
    parser = ruleset.new_parser();
    bufferer = new taglex.SourceBufferer(parser, taglex.ROOT);
    setup_bufferer(bufferer);
    text = "a *b c d---* e--- f u{g h *i j* k} l m n o";
    parser.write(text);
    bufferer.flush();

    // ensure source gets replicated
    var buffers = ["a *b c d---*", " e--- f u{g h *i j* k}", " l m n o"];
    test.deepEqual(buffers, source_buffer_result);

    // check node emissions
    result = [ [ 'TEXT', 'a ' ],
                [ 'OPEN', 'emphasis' ], [ 'TEXT', 'b c d' ], [ 'SYMBOL', 'dash'], [ 'CLOSE', 'emphasis' ],
                [ 'TEXT', ' e'], [ 'SYMBOL', 'dash'],
                [ 'TEXT', ' f ' ],
                [ 'OPEN', 'underline' ], [ 'TEXT', 'g h ' ],
                    [ 'OPEN', 'emphasis' ], [ 'TEXT', 'i j' ], [ 'CLOSE', 'emphasis' ],
                    [ 'TEXT', ' k' ], [ 'CLOSE', 'underline' ],
                [ 'TEXT', ' l m n o' ] ];
    test.deepEqual(result, node_contents);

    test.done();
};


exports.test_readme_examples = function (test) {

    // Examples from the README copied and pasted here

    var taglex = require('./index');
    ///var sys = require('sys');
    r = ''; ///
    sys = {stdout: {write: function (s) { r += s; }}}///

    var ruleset = new taglex.TagRuleSet({ ignore_case: true });
    ruleset.add_tag({
        name: 'table',
        ignore_text: true,
        open: '{{{', close: '}}}',
        parents: ['root'],
        payload: {start: '<table>', finish: '</table>'}
    });

    ruleset.add_tag({
        name: 'row',
        ignore_text: true,
        open: '[', close: ']',
        parents: ['table'],
        payload: {start: '<tr>', finish: '</tr>'}
    });

    ruleset.add_tag({
        name: 'cell',
        open: '[', close: ']',
        parents: ['row'],
        payload: {start: '<td>', finish: '</td>'}
    });

    ruleset.add_tag({
        name: 'i',
        open: '[', close: ']',
        parents: ['i', 'cell'],
        payload: {start: '<i>', finish: '</i>'}
    });

    var parser = ruleset.new_parser();
    parser.on('tag_open', function (payload, token) {
        sys.stdout.write(payload.start);
    });

    parser.on('text_node', function (text) {
        sys.stdout.write(text.replace(/</g, "&lt;")); // escape
    });

    parser.on('tag_close', function (payload, token) {
        sys.stdout.write(payload.finish);
    });

    parser.write("Outside the table I can freely use [] characters.\n");
    parser.write("Here is a table example:\n{{{ (ignored text)");
    parser.write("[ [ cell 1 ] [[[ cell 2 ]]] [ cell 3 ] ]\n");
    parser.write("[ [ cell 4 ] [ cell 5 ] [ cell 6 ] ]");
    parser.write("}}}");

    // Would output:
    var expected = [
        "Outside the table I can freely use [] characters.",
        "Here is a table example:",
        "<table><tr><td> cell 1 </td><td><i><i> cell 2 </i></i></td><td> cell 3 </td></tr>" +
        "<tr><td> cell 4 </td><td> cell 5 </td><td> cell 6 </td></tr></table>",
    ].join('\n');


    test.equal(expected, r);

    ruleset = new taglex.TagRuleSet({ ignore_case: true });
    ruleset.add_tag({
        name: 'italic',
        open: '*',
        close: '*',
        parents: ['root'],
        aliases: [['_', '_'], ['i:', ':i']],
        payload: {start: '<i>', finish: '</i>'}
    });


    r = '';
    parser = ruleset.new_parser();
    parser.on('tag_open', function (payload, token) {
        sys.stdout.write(payload.start);
    });

    parser.on('text_node', function (text) {
        sys.stdout.write(text.replace(/</g, "&lt;")); // escape
    });

    parser.on('tag_close', function (payload, token) {
        sys.stdout.write(payload.finish);
    });

    parser.write("This is an *example* of a small I:regular");
    parser.write(" language:I");

    // Would output:
    expected = 'This is an <i>example</i> of a small <i>regular language</i>';

    test.equal(expected, r);
    test.done();
};


exports.test_examples = function (test) {
    var mockery = require('mockery');
    mockery.enable();
    mockery.registerMock('taglex', taglex);
    mockery.warnOnUnregistered(false);
    var out = [];
    mockery.registerMock('sys', {
        print: function (s) { out.push(s); },
    });

    var check = function (name) {
        out = [];
        var example = require('./examples/' + name);
        test.deepEqual(out.join("").split("\n"), example.expected, 'Example ' + name);
    };

    check('lexing_regular_language');
    check('markdown');

    mockery.disable();
    test.done();
};

