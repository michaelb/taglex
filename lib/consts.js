exports.ROOT = 'tag_root';
exports.TEXT_NODE = 1;
exports.TAG_NODE  = 2;
exports.POP        = 1;
exports.DOUBLE_POP = 2;
exports.NOOP       = 3;


var reverse = {};
reverse[exports.POP] = 'POP';
reverse[exports.DOUBLE_POP] = 'DOUBLE_POP';
reverse[exports.NOOP] = 'NOOP';
exports.reverse = reverse;

