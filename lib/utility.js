var consts = require('./consts');
var pjson = require('../package.json');


exports.extend = function (origin, add) {
    var keys = Object.keys(add || {}), i = keys.length;
    while (i--) { origin[keys[i]] = add[keys[i]]; }
    return origin;
};

exports.arg_coerce = function (arg) {
    return Array.prototype.slice.call(arg);
};

exports.escape_for_regexp = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

exports.serialize_ruleset = function (ruleset, opts) {
    opts = opts || {};
    // Serializes ruleset, as state transition
    // FORMAT:
    //"state, token [(regexp)], new_state"
    var rules = ruleset._reference_grammar.map(function (r) {
        return [
            r[1], // state
            JSON.stringify(r[0]) + (r[3] ? ' ('+r[3].toString()+')' : ''), // token (regexp)
            consts.reverse[r[2]] || 'PUSH('+r[2]+')', // next state
        ].join(" -> ");
    });

    var collapsers = ruleset._reference_grammar_collapse.map(function (r) {
        if (!opts.expand_collapsers) {
            return [
                'ANY_DESCENDENT_OF(' + r[1] + ')', // state
                JSON.stringify(r[0]) + (r[2] ? ' ('+r[2].toString()+')' : ''), // token (regexp)
                'POP_UNTIL(' + r[1] + ')', // state
            ].join(" -> ");
        }

        var res = [], collapse_to = r[1];
        var descendant_states = ruleset.get_descendant_states(collapse_to);
        for (var i in descendant_states) {
            var d_state = descendant_states[i];
            if (d_state === collapse_to) { continue; }
            res.push([
                d_state, // state
                JSON.stringify(r[0]) + (r[3] ? ' ('+r[3].toString()+')' : ''), // token (regexp)
                'POP_UNTIL(' + collapse_to + ')', // state
            ].join(" -> "));
        };
        return res.join("\n");
    });
    var result = rules.concat(collapsers);
    result.sort();

    return [
        'TagLex Grammar ' + pjson.version,
        '',
        'Rules:',
        result.join("\n"),
    
    ].join("\n");

};


