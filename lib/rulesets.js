"use strict";
var util = require('util');
var consts = require('./consts');
var lexers = require('./lexers');
var parsers = require('./parsers');
var utility = require('./utility');


// Streaming tokenizer based on tags and state stack
var RuleSet = function (options) {
    var opts = utility.extend({
        ignore_case: false,
        normalizer: null,
        lexer_class: lexers.Lexer,
        parser_class: parsers.StackParser,
        close_at_end: true,
        //messy_stack_collapse: false,
        //forbid_loose_text: false,
    }, options);

    this.state_edges = {};
    this.rollup_states = {};
    this.regexps = null;
    this.collapsers = {};
    this.opts = opts;
    this.ignore_case = opts.ignore_case;
    this.normalizer = opts.normalizer;

    // store info while building via add
    this._states = {};
    this._regexps = {};
    this._collapsers = {};
    this._direct_edges = {};
    this._reverse_edges = {};
    this._cache = {};

    // for serialization
    this._reference_grammar = [];
    this._reference_grammar_collapse = [];
};

RuleSet.prototype.compile = function () {
    // First, compile collapses into standard state transitions
    this.collapsers = {};
    for (var token in this._collapsers) {
        var _pair = this._collapsers[token],
                collapse_to = _pair[0],
                regexp = _pair[1];
        //var parent_states = this.get_ancestor_states(tagname);
        var descendant_states = this.get_descendant_states(collapse_to);
        for (var i in descendant_states) {
            var d_state = descendant_states[i];

            // self-collapser is meaningless, skip
            if (d_state === collapse_to) { continue; }

            // add token to be compiled into relevant regexp
            this._add_regexp(token, d_state, regexp);

            // build up collapsers
            if (!this.collapsers[d_state]) { this.collapsers[d_state] = {}; }
            if (!this.collapsers[d_state][token]) { this.collapsers[d_state][token] = {}; }
            this.collapsers[d_state][token][collapse_to] = true;
        }
    };

    this.regexps = {};
    for (var state_name in this._states) {
        var patterns = [];
        this._states[state_name].forEach(function (token_name) {
            patterns.push(utility.escape_for_regexp(token_name));
        });

        // Sort patterns by length, so that longer is first
        // (prevents lexing "****" as "*", "*", "*", "*", not "**", "**")
        patterns.sort(function (a, b) { return b.length - a.length; });

        // Now lets prefix the patterns with the regexs
        patterns = this._regexps[state_name].concat(patterns);

        // create the regex for this state context
        var exp = '(' + patterns.join('|') + ')';
        var regexp = new RegExp(exp, this.ignore_case ? 'i' : '');
        this.regexps[state_name] = regexp;
    }
};

RuleSet.prototype.get_collapsers = function (d_state, token) {
    if (!(d_state in this.collapsers)) { return null; }
    if (!(token in this.collapsers[d_state])) { return null; }
    return this.collapsers[d_state][token];
};

RuleSet.prototype.add = function (token, state, new_state, regexp) {
    // Reset everything, setup any objs
    this.regexps = null; // reset so it needs to be re-compiled
    this._cache = {};    // reset internal cache too

    // set default values
    if (typeof state === "undefined") { state = consts.ROOT; }
    if (typeof new_state === "undefined") { new_state = consts.NOOP; }

    // set up various association tables
    if (!this._states[state]) { this._states[state] = []; }
    if (!this._regexps[state]) { this._regexps[state] = []; }
    if (!this._direct_edges[state]) { this._direct_edges[state] = []; }
    if (!this._reverse_edges[new_state]) { this._reverse_edges[new_state] = []; }
    if (!this.state_edges[state]) { this.state_edges[state] = {}; }

    // Set up reverse and direct edges, used by collapse feature
    if (typeof new_state !== 'number') {
        this._direct_edges[state].push(new_state);
        this._reverse_edges[new_state].push(state);
    }

    // Set up state transition edge
    this.state_edges[state][token] = new_state;

    this._add_regexp(token, state, regexp);

    this._reference_grammar.push(utility.arg_coerce(arguments));
};

RuleSet.prototype._add_regexp = function (token, state, regexp) {
    // Set up regexp if it exists
    if (regexp) {
        if (!this.normalizer) {
            throw new Error("Attempting to add a regexp token "+
                "while no normalizer has been specified: " + token);
        }
        this._regexps[state].push(regexp);
    } else {
        // otherwise just add token
        this._states[state].push(token);
    }
};

/*
Recurses forward through state transition edges adding in all
possible children states (e.g. "child" and "grandchild" tags)
*/
RuleSet.prototype.get_descendant_states = function (state, desc_states) {
    /*
    // memoization broken because of non-functional implementation
    // of this function (e.g. state is shared with 2nd arg)
    if (!this._cache.dstates) { this._cache.dstates = {}; }
    if (this._cache.dstates && this._cache.dstates[state]) {
        return this._cache.dstates[state]; } */

    desc_states = desc_states || {};
    (this._direct_edges[state] || [])
        .filter(function (s) { return !(s in desc_states); })
        .forEach(function (s) {
            desc_states[s] = true;
            this.get_descendant_states(s, desc_states);
        }, this);

    var result = Object.keys(desc_states);
    //this._cache.dstates[state] = result;
    return result;
};

/*
Recurses backwardward through state transition edges adding in all
possible parent or ancestor states (e.g. "parent" tags)
*/
RuleSet.prototype.get_ancestor_states = function (state, parent_states) {
    /*if (!this._cache.pstates) { this._cache.pstates = {}; }
    if (this._cache.pstates && this._cache.pstates[state]) {
        return this._cache.pstates[state]; } // return memoized version*/

    parent_states = parent_states || {};
    (this._reverse_edges[state] || [])
        .filter(function (s) { return !(s in parent_states); })
        .forEach(function (s) {
            parent_states[s] = true;
            this.get_ancestor_states(s, parent_states);
        }, this);

    var result = Object.keys(parent_states);
    //this._cache.pstates[state] = result;
    return result;
};

RuleSet.prototype.add_stack_collapser = function (token, collapse_to_state, regexp) {
    this._collapsers[token] = [collapse_to_state, regexp || null];
    this._reference_grammar_collapse.push(utility.arg_coerce(arguments));
};


RuleSet.prototype.set_rollup_state = function (state) {
    this.rollup_states[state] = true;
};

RuleSet.prototype.new_lexer = function (default_state, on_func) {
    if (this.regexps === null) {
        this.compile();
    }

    if (!default_state) {
        default_state = consts.ROOT; // default for root state
    }

    return new this.opts.lexer_class(this, default_state, on_func);
};


RuleSet.prototype._new_streamer = function (klass, default_state, on_func) {
    if (this.regexps === null) {
        this.compile();
    }

    if (!default_state) {
        default_state = consts.ROOT; // default for root state
    }

    return new klass(this, default_state, on_func);
};

RuleSet.prototype.new_parser = function (default_state, on_func) {
    return this._new_streamer(this.opts.parser_class, default_state, on_func);
};

RuleSet.prototype.new_lexer = function (default_state, on_func) {
    return this._new_streamer(this.opts.lexer_class, default_state, on_func);
};

// Improved ruleset with handy utility functions for tags
var TagRuleSet = function (options) {
    var opts = utility.extend({
        parser_class: parsers.TagParser,
        root_ignore_text: false,
    }, options);
    RuleSet.call(this, opts);
    this.by_state = {};
    this.symbol_by_token = {};
    this.state_ignore_text = {};
    this._taginfos = [];
    this._symbolinfos = [];

    // add in if we ignore text in "ROOT" state
    this.state_ignore_text[consts.ROOT] = opts.root_ignore_text;

    //this.LexerClass = lexers.TagLexer;
};
util.inherits(TagRuleSet, RuleSet);

var TAG_STATE_PREFIX = "tag_";
TagRuleSet.prototype.add_tag = function (taginfo) {
    this.regexps = null; // reset so it needs to be re-compiled
    this._cache = {};    // reset internal cache too

    var tag_name = taginfo.name;
    var state_name = TAG_STATE_PREFIX + tag_name;
    //this.tags[tag_name] = taginfo;

    if (!taginfo.aliases) { taginfo.aliases = []; }
    if (!taginfo.payload) { taginfo.payload = tag_name; }

    if ("open" in taginfo) {
        taginfo.aliases.push([taginfo.open, taginfo.close]);
    }

    this._taginfos.push(taginfo);
};

TagRuleSet.prototype.compile = function (taginfo) {
    this._taginfos.forEach(this._precompile_taginfo, this);
    this._symbolinfos.forEach(this._precompile_symbolinfo, this);

    // call super's compile
    RuleSet.prototype.compile.call(this);
};

var _flatten = function (arr) {
    if (typeof arr[0] !== "string") {
        return arr.reduce(function(a, b) { return a.concat(b); });
    }
    return arr;
};

var _prep_match = function (match) {
    return typeof match === "string"
                ? { token: match, re: null, }
                : match;
};

TagRuleSet.prototype._precompile_taginfo = function (taginfo) {
    var tag_name = taginfo.name;
    var state_name = TAG_STATE_PREFIX + tag_name;

    // in case it's an array of arrays
    taginfo.parents = _flatten(taginfo.parents);

    // set up ignore text and rollup
    this.state_ignore_text[state_name] = !!taginfo.ignore_text;
    if (taginfo.rollup) {
        this.set_rollup_state(state_name);
        taginfo.parents.push(tag_name); // making self contained
    }


    taginfo.aliases.forEach(function (_pair) {
        var open_match = _prep_match(_pair[0]);
        var close_match = _prep_match(_pair[1]);

        // Add opening tag
        taginfo.parents.forEach(function (parent_tag) {
            var parent_state_name = TAG_STATE_PREFIX + parent_tag;
            this.add(open_match.token, parent_state_name, state_name, open_match.re);
        }, this);

        // Add closing tag to this state
        this.add(close_match.token, state_name, consts.POP, close_match.re);

        if (taginfo.force_close) {
            // Force children to close if this closes
            this.add_stack_collapser(close_match.token, state_name, close_match.re);
        }

        // hook state name
        this.by_state[state_name] = taginfo;

    }, this);
};

TagRuleSet.prototype.add_symbol = function (symbolinfo) {
    var tag_name = symbolinfo.name;
    //var state_name = TAG_STATE_PREFIX + tag_name;
    if (!symbolinfo.aliases) { symbolinfo.aliases = []; }
    if (symbolinfo.symbol) { symbolinfo.aliases.push(symbolinfo.symbol); }
    if (!symbolinfo.payload) { symbolinfo.payload = tag_name; }

    this._symbolinfos.push(symbolinfo);
};

TagRuleSet.prototype._precompile_symbolinfo = function (symbolinfo) {
    var tag_name = symbolinfo.name;
    //var state_name = TAG_STATE_PREFIX + tag_name;

    // in case it's an array of arrays
    symbolinfo.parents = _flatten(symbolinfo.parents);
 
    symbolinfo.aliases.forEach(function (match) {
        // Add opening tag to all parent tags
        var match_obj = _prep_match(match);
        symbolinfo.parents.forEach(function (parent_tag) {
            var parent_state_name = TAG_STATE_PREFIX + parent_tag;
            if (!this.symbol_by_token[parent_state_name]) {
                this.symbol_by_token[parent_state_name] = {};
            }
            this.symbol_by_token[parent_state_name][match_obj.token] = symbolinfo;
            this.add(match_obj.token, parent_state_name, consts.NOOP, match_obj.re);
            //   add(token,           state,             new_state, regexp) {
        }, this);
    }, this);
};

exports.RuleSet = RuleSet;
exports.TagRuleSet = TagRuleSet;

