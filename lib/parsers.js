"use strict";
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var utility = require('./utility');
var consts = require('./consts');
var lexers = require('./lexers');


// pull out some global consts
var TEXT_NODE = consts.TEXT_NODE,
    TAG_NODE = consts.TAG_NODE, POP = consts.POP,
    DOUBLE_POP = consts.DOUBLE_POP, NOOP = consts.NOOP;


/////////////////////////////////////////////// StackParser
var StackParser = function (ruleset, default_state, on_func) {
    lexers.Lexer.call(this, ruleset, default_state, on_func);
    this.reset();
};

util.inherits(StackParser, lexers.Lexer);

StackParser.prototype.reset = function () {
    this.stack = [];
    this.stack_obj = {};
};

/*
Feed text into the parser
*/
StackParser.prototype.write = function (text) {
    //console.log("------------", text);
    var res = { text: text, next_state: this.state };
    while (res.text.length > 0) {
        this.state = this.peak();
        res = this._process(res.text, this.state);
        if (res === null) { break; }
        //console.log(res.next_state + '', "|", res.token, "|", res.text);

        this._transition_state_stack(
            this.state, res.next_state, res.initial_text, res.normalized, res.token);
    }
};

StackParser.prototype.peak = function () {
    var length = this.stack.length;
    return length > 0 ? this.stack[length-1] : this.default_state;
};

StackParser.prototype._transition_state_stack = function (state, next_state, initial_text, normalized, token) {
    /////////////////////////////////
    // Collapse feature logic
    // A collapse action pops all the way up to target
    // avoid double match for identical transitions
    if (next_state !== POP && next_state !== DOUBLE_POP) {
        var collapse_to = this.ruleset.get_collapsers(state, normalized);
        if (collapse_to) {
            var to_collapse = this._split_stack_at(collapse_to);

            if (to_collapse) {

                // first emit initial_text to keep it from getting
                // out of order
                if (initial_text.length > 0) {
                    this.emit("token", TEXT_NODE, initial_text);
                }

                initial_text = ''; // clear so we don't emit twice

                to_collapse.reverse().forEach(function (state) {
                    this.emit("stack_pop", state);
                }, this);
                state = this.peak();
                next_state = POP;
            } else {
                // we actually don't have this state in our stack, which means
                // this match must be a false positive
                // BUG: emits too many text nodes at this point :(
                this.emit("token", TEXT_NODE, initial_text + token);
                return;
            }
        }
    }
    /////////////////////////////////

    // Emit the prefix text, and the token
    if (initial_text.length > 0) {
        this.emit("token", TEXT_NODE, initial_text);
    }
    this.emit("token", TAG_NODE, normalized, token);

    /////////////////////////////////
    // Combine state logic
    if (next_state !== POP && next_state !== DOUBLE_POP) {
        if (next_state === state && state in this.ruleset.rollup_states) {
            next_state = NOOP;
        }
    };

    /////////////////////////////////
    // Perform state transition
    if (next_state === POP) {
        this.stack.pop();
        this.emit("stack_pop", state, token, normalized);

    } else if (next_state === DOUBLE_POP) {
        this.emit("stack_pop", this.stack.pop(), token, normalized);
        this.emit("stack_pop", this.stack.pop(), token, normalized);

    } else if (next_state === NOOP) {
        // No state transition
        this.emit("stack_pass", normalized, token, state);

    } else if (next_state === null) {
        this.emit("error", new Error("Entered unknown state with token "+
                                     JSON.stringify(token) +
                                     " normalized: " + JSON.stringify(normalized)));

    } else {
        if (typeof next_state !== "string") {
            this.emit("error", new Error("Non-string state: " + next_state));
        }

        this.stack.push(next_state);
        this.emit("stack_push", next_state, state, token, normalized);
    }

    // Finally, return remaining text
    //return remaining_text;
};

StackParser.prototype._split_stack_at = function (search_states) {
    // search_states, obj containing as keys the states to split
    // the stack at
    var i = this.stack.length-1; // don't ever split at top
    while (i-- > 0) {
        if (this.stack[i] in search_states) {
            // knock off stack at this point (splice splits in
            // place, and returns half)
            return this.stack.splice(i+1);
        }
    }
    return null;
};

/////////////////////////////////////////////// TagParser

// Improved version of StackParser that emits more info based on TagRuleSet
var TagParser = function (ruleset, default_state, emit) {
    StackParser.call(this, ruleset, default_state, emit);
    this._prep_events();
};
util.inherits(TagParser, StackParser);

TagParser.prototype._prep_events = function () {
    var ruleset = this.ruleset;
    var me = this;
    var combining = null;
    this.current_tag = null;


    /// helper function that checks if we are combining states
    var _check_combining = function (state) {
        var res = false;

        if (combining) {
            if (state && state === combining.state) {
                res = true;
            } else {
                // emit "late" tag close
                var s = combining.state, t = combining.token,
                    n = combining.normalized;
                combining = null;
                me.emit('stack_pop', s, t, n, true);
            }
            combining = null;
        }
        return res;
    };

    this.on('stack_push', function (next_state, state, token, normalized) {
        var taginfo = ruleset.by_state[next_state];
        me.current_tag = taginfo.payload;
        if (!taginfo.combine) {
            _check_combining(false);
        } else if (_check_combining(next_state)) {
            this.emit('tag_rolled_up', next_state, token, normalized);
            return;
        }

        if (!taginfo) {
            this.emit('tag_unknown', next_state, token, normalized);
        } else {
            this.emit('tag_open', taginfo.payload, token, normalized);
        }
    });


    this.on('stack_pop', function (state, token, normalized, skip_combining) {
        _check_combining(false);

        var taginfo = ruleset.by_state[state];
        me.current_tag = taginfo.payload;

        if (!taginfo) {
            this.emit('tag_unknown', state, token, normalized);
            return;
        } 

        ///// Combine feature
        if (taginfo.combine && !skip_combining) {
            combining = { state: state, token: token,
                            normalized: normalized, };
            return;
        }
        /////

        this.emit('tag_close', taginfo.payload, token, normalized);

        ///// One child feature
        var parent_tag = ruleset.by_state[this.peak()] || null;
        if (parent_tag && parent_tag.one_child) {
            // force a repeat
            this.emit("stack_pop", this.stack.pop(), token, normalized);
        }
        /////
    });

    this.on('stack_pass', function (normalized, token, state) {
        _check_combining(false);
        //console.log("STACK PASS", taginfo);
        var taginfo = ruleset.symbol_by_token[state] &&
                        ruleset.symbol_by_token[state][normalized];
        if (!taginfo) {
            this.emit('symbol_unknown', token);
        } else {
            this.emit('symbol', taginfo.payload);
        }
    });

    this.on('token', function (node_type, content) {
        if (TEXT_NODE === node_type) {
            _check_combining(false);

            if (ruleset.state_ignore_text[this.state]) {
                // ignore text within this state
                this.emit("text_ignored", content);
            } else {
                this.emit("text_node", content);
            }
        }
    });
};

TagParser.prototype.end = function () {
    if (this.ruleset.opts.close_at_end) {
        this.stack.reverse().forEach(function (state) {
            this.emit("stack_pop", state);
        }, this);
    }
    this.reset();
};


exports.StackParser = StackParser;
exports.TagParser = TagParser;

