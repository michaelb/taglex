var utility = require('./utility');


var TagClassManager = function () {
    this.classes = {
        'root': ['root'], // default
    };
};

/*
Adds tag_name to class(es).

(tag_name, classes..)
*/
TagClassManager.prototype.add = function () {
    var args = utility.arg_coerce(arguments);
    var name = args.shift();
    args.forEach(function (classname) {
        if (!this.classes[classname]) {
            this.classes[classname] = [];
        }

        this.classes[classname].push(name);
    }, this);
};


TagClassManager.prototype._get = function (classname) {
    if (!this.classes[classname]) {
        this.classes[classname] = [];
    }

    return this.classes[classname];
};

TagClassManager.prototype.get = function () {
    return Array.prototype.map.call(arguments, this._get, this);
};

exports.TagClassManager = TagClassManager;

