"use strict";
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var utility = require('./utility');


// Optional classes which can be used to wrap the parser, to modify the stream
// of events

// Extends TagParser, and emits single "source_buffer" events at a given stack
// level, containing full source code. Kind of a kludge for a very particular
// use case where you want to keep the source code handy of block-level
// elements (e.g., writing an editor where elements are editable at a certain
// level, and you want to attach it to data- attributes)
var SourceBufferer = function (parser, target_state) {
    EventEmitter.call(this);
    this.parser = parser;
    this.target_state = target_state;
    this.clear();
    this._prep_tree_events();
};
util.inherits(SourceBufferer, EventEmitter);

SourceBufferer.prototype.write = function () {
    this.parser.apply(this.parser, arguments);
};

SourceBufferer.prototype.clear = function () {
    this.event_buffer = [];
    this.source_buffer = [];
};

SourceBufferer.prototype.flush = function () {
    // Emit the source code
    this.emit('source_buffer', this.source_buffer.join(''));

    // Emit the event buffer
    var length = this.event_buffer.length;
    var i = 0;
    while (i < length) {
        this.emit.apply(this, this.event_buffer[i++]);
    }

    this.clear();
};

/*
Analogous to Parser.end(), first ends the internal parser, then flushes
whatever might remain
*/
SourceBufferer.prototype.end = function () {
    this.parser.end();
    this.flush();
};

SourceBufferer.prototype._prep_tree_events = function () {
    var me = this;

    this.parser.on('token', function (type, v) {
        me.source_buffer.push(v); // keep track of source
    });

    var make_tree_event = function (name) {
        return function () {
            var args = utility.arg_coerce(arguments);
            args.unshift(name);
            me.event_buffer.push(args);
        };
    };

    this.parser.on('text_node', make_tree_event("text_node"));
    this.parser.on('tag_open', make_tree_event("tag_open"));
    this.parser.on('symbol', make_tree_event("symbol"));

    this.parser.on('tag_close', make_tree_event("tag_close"));
    // and add extra logic:
    this.parser.on('tag_close', function () {
        if (me.parser.peak() === me.target_state) {
            me.flush();
        }
    });
};

exports.SourceBufferer = SourceBufferer;

